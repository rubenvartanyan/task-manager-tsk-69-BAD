package ru.vartanyan.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.IRepository;
import ru.vartanyan.tm.dto.Project;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


public interface IProjectRepository extends IRepository<Project> {

    void deleteAllByUserId(@NotNull String userId);

    @Nullable
    Project findOneById(@Nullable final String id);

    Project findOneByNameAndUserId(@Nullable final String userId,
                                   @Nullable final String name);

    void removeOneById(@Nullable final String id);

    List<Project> findAll();

    List<Project> findAllByUserId(@Nullable final String userId);

    @Nullable
    Project findOneByIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    );

    void removeOneByNameAndUserId(@Nullable final String userId,
                                  @Nullable final String name);

    void removeOneByIdAndUserId(@Nullable final String userId,
                                @NotNull final String id);

}

