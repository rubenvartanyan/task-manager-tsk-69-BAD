package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.configuration.ClientConfiguration;
import ru.vartanyan.tm.marker.IntegrationCategory;


import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final ProjectEndpoint projectEndpoint = context.getBean(ProjectEndpoint.class);

    private SessionDTO session;

    @Before
    @SneakyThrows
    public void before() {
        session = sessionEndpoint.openSession("admin", "admin");
        projectEndpoint.clearProjects(session);
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addProjectTest() {
        final String projectName = "nameTest";
        final String projectDescription = "nameTest";
        projectEndpoint.addProject(projectName, projectDescription, session);
        final ProjectDTO project = projectEndpoint.findProjectByName(projectName, session);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getDescription());
        Assert.assertEquals(projectDescription, project.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllProjectTest() {
        projectEndpoint.addProject("project1", "description1", session);
        projectEndpoint.addProject("project2", "description2", session);
        projectEndpoint.addProject("project3", "description3", session);
        Assert.assertEquals(3, projectEndpoint.findAllProjects(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectByIdTest() {
        projectEndpoint.clearProjects(session);
        Assert.assertEquals(0, projectEndpoint.findAllProjects(session).size());
        projectEndpoint.addProject("AA", "BB", session);
        List<ProjectDTO> projectList = projectEndpoint.findAllProjects(session);
        Assert.assertEquals(1, projectList.size());
        final ProjectDTO project = projectList.get(0);
        Assert.assertEquals("AA", project.name);
        final ProjectDTO projectFound = projectEndpoint.findProjectById(project.getId(), session);
        Assert.assertEquals("AA", projectFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByIndexTest() {
        Assert.assertEquals(0, projectEndpoint.findAllProjects(session).size());
        projectEndpoint.addProject("AA", "BB", session);
        final ProjectDTO projectFound = projectEndpoint.findProjectByIndex(0, session);
        Assert.assertEquals("AA", projectFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByNameTest() {
        Assert.assertEquals(0, projectEndpoint.findAllProjects(session).size());
        projectEndpoint.addProject("AA", "BB", session);
        final ProjectDTO projectFound = projectEndpoint.findProjectByName("AA", session);
        Assert.assertEquals("AA", projectFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectOneByIdTest() {
        projectEndpoint.clearProjects(session);
        Assert.assertEquals(0, projectEndpoint.findAllProjects(session).size());
        projectEndpoint.addProject("LLL", "LLL", session);
        List<ProjectDTO> projectList = projectEndpoint.findAllProjects(session);
        final ProjectDTO project = projectList.get(0);
        projectEndpoint.removeProjectById(project.getId(), session);
        Assert.assertEquals(0, projectEndpoint.findAllProjects(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectByNameTest() {
        Assert.assertEquals(0, projectEndpoint.findAllProjects(session).size());
        projectEndpoint.addProject("AA", "BB", session);
        projectEndpoint.removeProjectByName("AA", session);
        Assert.assertEquals(0, projectEndpoint.findAllProjects(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectByIndexTest() {
        Assert.assertEquals(0, projectEndpoint.findAllProjects(session).size());
        projectEndpoint.addProject("AA", "BB", session);
        projectEndpoint.removeProjectByIndex(0, session);
        Assert.assertEquals(0, projectEndpoint.findAllProjects(session).size());
    }

}
