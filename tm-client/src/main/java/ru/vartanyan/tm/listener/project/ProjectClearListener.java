package ru.vartanyan.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractProjectListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;

@Component
public class ProjectClearListener extends AbstractProjectListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-clearTasks";
    }

    @Override
    public String description() {
        return "Clear all projects";
    }

    @Override
    @EventListener(condition = "@projectClearListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[PROJECT CLEAR]");
        projectEndpoint.clearProjects(session);
        System.out.println("[OK]");
    }

}
