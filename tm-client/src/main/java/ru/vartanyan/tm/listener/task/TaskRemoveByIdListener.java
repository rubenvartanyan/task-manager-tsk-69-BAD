package ru.vartanyan.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractTaskListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class TaskRemoveByIdListener extends AbstractTaskListener {

    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove task by Id";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[REMOVE TASK]");
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final SessionDTO session = bootstrap.getSession();
        System.out.println("[ENTER ID]");
        @NotNull final String id = TerminalUtil.nextLine();
        taskEndpoint.removeTaskById(id, session);
        System.out.println("[TASK REMOVED]");
    }

}
