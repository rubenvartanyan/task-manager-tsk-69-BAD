package ru.vartanyan.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractListener;

@Component
public class VersionListener extends AbstractListener {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "show-version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version";
    }

    @Override
    @EventListener(condition = "@versionListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[VERSION]");
        System.out.println(propertyService.getApplicationVersion());
    }

}
