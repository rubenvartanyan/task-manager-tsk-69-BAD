
package ru.vartanyan.tm.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-04-10T17:47:19.087+03:00
 * Generated source version: 3.4.2
 */

@WebFault(name = "NoSuchUserException", targetNamespace = "http://endpoint.tm.vartanyan.ru/")
public class NoSuchUserException_Exception extends java.lang.Exception {

    private ru.vartanyan.tm.endpoint.NoSuchUserException faultInfo;

    public NoSuchUserException_Exception() {
        super();
    }

    public NoSuchUserException_Exception(String message) {
        super(message);
    }

    public NoSuchUserException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public NoSuchUserException_Exception(String message, ru.vartanyan.tm.endpoint.NoSuchUserException noSuchUserException) {
        super(message);
        this.faultInfo = noSuchUserException;
    }

    public NoSuchUserException_Exception(String message, ru.vartanyan.tm.endpoint.NoSuchUserException noSuchUserException, java.lang.Throwable cause) {
        super(message, cause);
        this.faultInfo = noSuchUserException;
    }

    public ru.vartanyan.tm.endpoint.NoSuchUserException getFaultInfo() {
        return this.faultInfo;
    }
}
